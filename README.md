Link do deploy desse projeto feito pelo Vercel: https://mochila-de-viagem-nu.vercel.app/

Uma página simples onde você pode fazer uma lista de itens para se organizar para uma viagem.
Fiz essa página seguindo os tutoriais da Alura.
Consegui aprender muito sobre javascript e como inserir e remover elementos de uma lista enquanto mantenho-a ordenada para poder acessar cada um de seus membros corretamente.
Também foi muito importante aprender sobre a local storage para armazenar e remover elementos de string direto na máquina do usuário para poder manter as informações da lista permanentes. É através dessa lista de string no local storage que a página recria os elementos html da lista toda vez que ela é carregada para que as informações que o usuário insere na página nunca sejam perdidas.
